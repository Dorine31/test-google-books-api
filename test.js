const express = require ('express')
//const axios = require ('axios')
const app = express()
const bodyParser = require ('body-parser')

//encoded
app.use(express.json())
app.use(bodyParser.json())
app.use(express.urlencoded({ extended: true }))
/*
// sans utiliser la page html
app.use('/', async (req, res)=> {
  let bookUrl = 'https://www.googleapis.com/books/v1/volumes?q='
  let search = await axios.get(bookUrl + 'python_3')
  console.log(search.data.items)
  res.send(search.data.items)
})*/

app.use("./index.html", (req, res, next)=> {
  if(res != "") {
    res.status(200)
  } else {
    res.status(404)
  }
  next()
})
app.use("/select", (req, res, next) => {
  console.log("req body: ",req.body.data)
  let data = req.body.data

  res.send(data)
  next()
})

//static pages
app.use(express.static('./static'))

//connected on port
const port = process.env.PORT || 3000

app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})